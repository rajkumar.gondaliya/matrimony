package com.aswdc_myapplication.model;

import java.io.Serializable;

public class UserModel implements Serializable {

    int UserId;
    String Name;
    String FatherName;
    String Email;
    int Gender;
    String SurName;
    String Hobbies;
    String Dob;
    String PhoneNumber;
    int LanguageID;
    int CityID;
    String Language;
    String City;
    int IsFavorite;

    public void setIsFavorite(int isFavorite) {
        IsFavorite = isFavorite;
    }

    public int getIsFavorite() {
        return IsFavorite;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getLanguage() {
        return Language;
    }

    public void setLanguage(String language) {
        Language = language;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getFatherName() {
        return FatherName;
    }

    public void setFatherName(String fatherName) {
        FatherName = fatherName;
    }

    public int getGender() {
        return Gender;
    }

    public void setGender(int gender) {
        Gender = gender;
    }

    public String getSurName() {
        return SurName;
    }

    public void setSurName(String surName) {
        SurName = surName;
    }

    public String getHobbies() {
        return Hobbies;
    }

    public void setHobbies(String hobbies) {
        Hobbies = hobbies;
    }

    public String getDob() {
        return Dob;
    }

    public void setDob(String dob) {
        Dob = dob;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    public int getLanguageID() {
        return LanguageID;
    }

    public void setLanguageID(int languageID) {
        LanguageID = languageID;
    }

    public int getCityID() {
        return CityID;
    }

    public void setCityID(int cityID) {
        CityID = cityID;
    }

    @Override
    public String toString() {
        return "UserModel{" +
                "UserId=" + UserId +
                ", Name='" + Name + '\'' +
                ", FatherName='" + FatherName + '\'' +
                ", Email='" + Email + '\'' +
                ", Gender=" + Gender +
                ", SurName='" + SurName + '\'' +
                ", Hobbies='" + Hobbies + '\'' +
                ", Dob='" + Dob + '\'' +
                ", PhoneNumber='" + PhoneNumber + '\'' +
                ", LanguageID=" + LanguageID +
                ", CityID=" + CityID +
                ", Language='" + Language + '\'' +
                ", City='" + City + '\'' +
                ", IsFavorite=" + IsFavorite +
                '}';
    }
}