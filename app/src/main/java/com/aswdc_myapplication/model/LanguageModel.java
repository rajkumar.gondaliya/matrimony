package com.aswdc_myapplication.model;

import java.io.Serializable;

public class LanguageModel implements Serializable {
    int LanguageID;
    String Name;

    public int getLanguageID() {
        return LanguageID;
    }

    public void setLanguageID(int languageID) {
        LanguageID = languageID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    @Override
    public String toString() {
        return "LanguageModel{" +
                "LanguageID=" + LanguageID +
                ", Name='" + Name + '\'' +
                '}';
    }
}
