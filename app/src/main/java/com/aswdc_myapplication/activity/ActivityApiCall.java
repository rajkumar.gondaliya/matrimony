package com.aswdc_myapplication.activity;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aswdc_myapplication.R;
import com.aswdc_myapplication.adapter.MovieListAdapter;
import com.aswdc_myapplication.api.ApiClient;
import com.aswdc_myapplication.api.ApiInterface;
import com.aswdc_myapplication.model.movie_list.MovieListModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityApiCall extends BaseActivity {

    private static final String API_KEY = "7e8f60e325cd06e164799af1e317d7a7";

    ArrayList<MovieListModel> movieList = new ArrayList<>();
    @BindView(R.id.rcvMovieList)
    RecyclerView rcvMovieList;

    MovieListAdapter adapter;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_api_call);
        ButterKnife.bind(this);
        getMovieListFromServer();
    }

    void setMoviesAdapter() {
        if (adapter == null) {
            adapter = new MovieListAdapter(this, movieList);
            rcvMovieList.setLayoutManager(new GridLayoutManager(this, 1));
            rcvMovieList.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
        }
    }

    void getMovieListFromServer() {
        progressDialog = ProgressDialog.show(this, getString(R.string.app_name), getString(R.string.lbl_pls_wait));
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.getTopRatedMovies(API_KEY);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    parseApiData(response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                setMoviesAdapter();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    void parseApiData(String jsonResponse) {
        try {
            JSONObject jsonObject = new JSONObject(jsonResponse);
            JSONArray array = jsonObject.getJSONArray("results");
            for (int i = 0; i < array.length(); i++) {
                JSONObject movieObject = array.getJSONObject(i);
                MovieListModel model = new MovieListModel();
                model.setAdult(movieObject.getBoolean("adult"));
                model.setBackdrop_path(movieObject.getString("backdrop_path"));
                JSONArray idsArray = movieObject.getJSONArray("genre_ids");
                ArrayList<Integer> genereIds = new ArrayList<>();
                for (int j = 0; j < idsArray.length(); j++) {
                    genereIds.add(idsArray.getInt(j));
                }
                model.setGenre_ids(genereIds);
                model.setId(movieObject.getInt("id"));
                model.setOriginal_language(movieObject.getString("original_language"));
                model.setOriginal_title(movieObject.getString("original_title"));
                model.setOverview(movieObject.getString("overview"));
                model.setPopularity(movieObject.getDouble("popularity"));
                model.setTitle(movieObject.getString("title"));
                model.setVideo(movieObject.getBoolean("video"));
                model.setVote_average(movieObject.getDouble("vote_average"));
                model.setPoster_path(movieObject.getString("poster_path"));
                model.setVote_count(movieObject.getInt("vote_count"));
                movieList.add(model);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
