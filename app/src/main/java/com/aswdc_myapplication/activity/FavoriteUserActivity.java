package com.aswdc_myapplication.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aswdc_myapplication.R;
import com.aswdc_myapplication.adapter.UserListAdapter;
import com.aswdc_myapplication.database.TblUser;
import com.aswdc_myapplication.model.UserModel;
import com.aswdc_myapplication.util.Constant;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FavoriteUserActivity extends BaseActivity {

    @BindView(R.id.rcvUserList)
    RecyclerView rcvUserList;

    ArrayList<UserModel> userList = new ArrayList<>();
    UserListAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);
        ButterKnife.bind(this);
        setUpActionBar(getString(R.string.lbl_userlist), true);
        setAdapter();
    }

    void setAdapter() {
        rcvUserList.setLayoutManager(new GridLayoutManager(this, 1));
        userList.addAll(new TblUser(this).getFavoriteUserList());
        adapter = new UserListAdapter(this, userList, new UserListAdapter.OnViewClickListener() {
            @Override
            public void OnDeleteClick(int position) {

            }

            @Override
            public void onFavoriteClick(int position) {
                int lastUpdatedUserID = new TblUser(FavoriteUserActivity.this).updateFavoriteStatus(0, userList.get(position).getUserId());
                if (lastUpdatedUserID > 0) {
                    sendFavoriteChangeBroadCast(userList.get(position));

                    userList.remove(position);
                    adapter.notifyItemRemoved(position);
                    adapter.notifyItemRangeChanged(0, userList.size());
                }
            }

            @Override
            public void OnItemClick(int position) {

            }
        });
        rcvUserList.setAdapter(adapter);
    }

    void sendFavoriteChangeBroadCast(UserModel userModel) {
        Intent intent = new Intent(Constant.FAVORITE_CHANGE_FILTER);
        intent.putExtra(Constant.USER_ID, userModel);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
}