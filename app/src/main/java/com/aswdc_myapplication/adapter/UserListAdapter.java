package com.aswdc_myapplication.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aswdc_myapplication.R;
import com.aswdc_myapplication.model.UserModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.UserHolder> {

    Context context;
    ArrayList<UserModel> userList;
    OnViewClickListener onViewClickListener;

    public UserListAdapter(Context context, ArrayList<UserModel> userList, OnViewClickListener onViewClickListener) {
        this.context = context;
        this.userList = userList;
        this.onViewClickListener = onViewClickListener;
    }

    public interface OnViewClickListener {
        void OnDeleteClick(int position);

        void onFavoriteClick(int position);

        void OnItemClick(int position);
    }

    @Override
    public UserHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new UserHolder(LayoutInflater.from(context).inflate(R.layout.view_row_user_list, null));
    }

    @Override
    public void onBindViewHolder(UserHolder holder, int position) {
        holder.tvFullName.setText(userList.get(position).getName() + " " + userList.get(position).getSurName());
        holder.tvLanguage.setText(userList.get(position).getLanguage() + " | " + userList.get(position).getCity());
        holder.tvDob.setText(userList.get(position).getDob());
        holder.ivFavoriteUser.setImageResource(userList.get(position).getIsFavorite() == 0 ? R.drawable.ic_outline_star_rate_black_48 : R.drawable.baseline_star_rate_black_24);

        holder.ivDeleteUser.setOnClickListener(view -> {
            if (onViewClickListener != null) {
                onViewClickListener.OnDeleteClick(position);
            }
        });

        holder.ivFavoriteUser.setOnClickListener(view -> {
            if (onViewClickListener != null) {
                onViewClickListener.onFavoriteClick(position);
            }
        });

        holder.itemView.setOnClickListener(view -> {
            if (onViewClickListener != null) {
                onViewClickListener.OnItemClick(position);
            }
        });
    }


    @Override
    public int getItemCount() {
        return userList.size();
    }

    class UserHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvFullName)
        TextView tvFullName;
        @BindView(R.id.tvLanguage)
        TextView tvLanguage;
        @BindView(R.id.tvDob)
        TextView tvDob;
        @BindView(R.id.ivDeleteUser)
        ImageView ivDeleteUser;
        @BindView(R.id.ivFavoriteUser)
        ImageView ivFavoriteUser;

        public UserHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
