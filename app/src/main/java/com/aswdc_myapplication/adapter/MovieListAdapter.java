package com.aswdc_myapplication.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aswdc_myapplication.R;
import com.aswdc_myapplication.model.movie_list.MovieListModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieListAdapter extends RecyclerView.Adapter<MovieListAdapter.MovieHolder> {

    Context context;
    ArrayList<MovieListModel> movieList;

    public MovieListAdapter(Context context, ArrayList<MovieListModel> movieList) {
        this.context = context;
        this.movieList = movieList;
    }

    @NonNull
    @Override
    public MovieHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MovieHolder(LayoutInflater.from(context).inflate(R.layout.view_row_movie_item, null));
    }

    @Override
    public void onBindViewHolder(@NonNull MovieHolder holder, int position) {
        holder.tvTitle.setText(movieList.get(position).getTitle());
        holder.tvRating.setText(String.format("%.2f", movieList.get(position).getPopularity()));
        holder.tvDescription.setText(movieList.get(position).getOverview());
    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }

    class MovieHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.tvDate)
        TextView tvDate;
        @BindView(R.id.tvRating)
        TextView tvRating;
        @BindView(R.id.tvDescription)
        TextView tvDescription;

        public MovieHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
